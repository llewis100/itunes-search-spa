const express = require('express')
const app = express()
const port = process.env.PORT || 9000;
const fetch = require('node-fetch');
const constants = require('./constants');
const path = require('path');
const cors = require("cors");
app.use(cors());

app.use(express.static("build"));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
})

app.get('/search', (req, res) => {
    const queryKeys = Object.keys(req.query);
    if (!queryKeys.length) {
        res.send(null);
        return;
    }
    const searchParams = new URLSearchParams();
    queryKeys.map(k => searchParams.append(k, req.query[k]));

    fetch(`${constants.API_BASE_URL}/search?${searchParams.toString()}`)
        .then((res) => res.json())
        .then((json) => {
            res.send(json.results);
        }).catch((err) => {
            res.send(err.message);
        })
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})