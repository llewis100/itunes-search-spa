# iTunes Search SPA
Hi! This is an SPA that shows results from searching the iTunes music store.

To get set up first, in the command line, run

```npm install```

then

```npm run-script build```

this will place the built app into the Express server, now

```cd server```

```npm install```

```npm start```

Now in the browser you should be able to visit localhost:9000 and see the packaged app served by node.

To run tests make sure you are in the src folder and run 

```npm test```

## Improvements
If I were to continue working on this the priorities for improvement would be:

• The Apple API doesn't allow for ranges to be selected, so in the interest of efficiency I opted to fetch the maximum 200 results and then lazy load the items and their images as the user scrolls. This seems to work well but the initial load is sligtly longer than is ideal. If this were an API could modify or request features I would opt for range selection.

• The lazy load feature as it stands could be improved, either by using a react library or making a custom hook. Unfortunately I was pushed for time.

• Given more time I would liked to have added [mocking of API calls](https://redux.js.org/usage/writing-tests#action-creators--thunks) to place custom items into the Listing component's grid and then use the React Testing Library getByText or similar to check mocked listings.