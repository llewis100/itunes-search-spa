interface queryParamsObj {
    term: string | number;
    [key: string]: string | number;
};

export const queryBuilder = (obj: queryParamsObj):string => {
    const queryKeys = Object.keys(obj);
    if (!queryKeys.length) return '';
    const searchParams = new URLSearchParams();
    queryKeys.map(k => searchParams.append(k, obj[k]));
    return searchParams.toString();
};