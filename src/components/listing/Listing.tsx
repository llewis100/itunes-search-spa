import React, { useState, useEffect, useRef } from 'react';
import { useAppSelector } from '../../app/hooks';
import {
    selectSearchResults,
    selectSearchStatus,
    selectQueryString
} from './listingSlice';

import CircularProgress from '@mui/material/CircularProgress';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Box from '@mui/material/Box';

const lazyLoadIncrement = 10;

export function Listing() {
    const searchResults = useAppSelector(selectSearchResults);
    const searchStatus = useAppSelector(selectSearchStatus);
    const queryString = useAppSelector(selectQueryString);

    const [displayCount, setDisplayCount] = useState(lazyLoadIncrement);
    const [displayList, setDisplayList] = useState([]);

    const gridContainer = useRef();

    useEffect(() => {
        setDisplayList(searchResults.slice(0, displayCount));
    }, [searchResults, displayCount]);

    window.onscroll = () => {
        if (!displayList.length) return;
        const userScrolledToBottom = gridContainer.current.getBoundingClientRect().bottom < window.innerHeight;
        userScrolledToBottom && setDisplayCount(displayCount + lazyLoadIncrement);
    };

    return (
        <Container sx={{ py: 8 }} maxWidth="md" id="resultsListing">
            {searchStatus === 'loading'
            ?
                <CircularProgress />
            :
                <>
                    <Grid ref={gridContainer} container spacing={4}>
                        {displayList && displayList.map((card) => (
                            <Grid item key={card.trackId} xs={12} sm={6} md={3}>
                                <Card sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
                                    <CardMedia
                                        component="img"
                                        image={card.artworkUrl100}
                                        alt="random"
                                    />
                                    <CardContent sx={{ flexGrow: 1 }}>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {card.trackName}
                                        </Typography>
                                        <Typography>
                                            {card.artistName}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                    {(searchStatus === 'idle' && !!queryString.length && !searchResults.length) &&
                        <Box>
                            <Container maxWidth="sm">
                                <Typography
                                    align="center"
                                    color="text.primary"
                                    gutterBottom
                                >
                                    No results found.
                                </Typography>
                            </Container>
                        </Box>
                    }
                </>
            }
        </Container>
    );
}
