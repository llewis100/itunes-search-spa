import { RootState } from '../../app/store';

export const selectSearchResults = (state: RootState) => state.searchResults.data;
export const selectSearchStatus = (state: RootState) => state.searchResults.status;
export const selectQueryString = (state: RootState) => state.searchResults.queryString;
