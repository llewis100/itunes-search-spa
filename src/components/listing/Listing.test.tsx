import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../app/store';
import { Listing } from './Listing';

test('renders learn react link', () => {
    const { container } = render(
        <Provider store={store}>
            <Listing />
        </Provider>
    );

    expect(container.firstChild.id === 'resultsListing').toBe(true);
});
