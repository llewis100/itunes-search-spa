import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { API } from '../../constants';
import { queryBuilder } from '../../utility';

export interface searchResultsState {
    queryString: string;
    data: any;
    status: 'idle' | 'loading' | 'failed';
}

const initialState: searchResultsState = {
    queryString: '',
    data: [],
    status: 'idle',
};

export const getSearchResults = createAsyncThunk(
    'searchResults/fetchResults',
    async (queryString: string, thunkAPI) => {
        const formattedQueryString = queryBuilder({ media: 'music', limit: 200, term: queryString });
        thunkAPI.dispatch(changeQueryString(formattedQueryString));
        const response = await fetch(`${API.SEARCH}${formattedQueryString}`)
            .then(response => response.json())
            .then(data =>  data)
            .catch(err => console.log(err.message));
        return response;
    }
);

export const searchResultsSlice = createSlice({
    name: 'searchResults',
    initialState,
    reducers: {
        changeQueryString: (state, action: PayloadAction<number>) => {
            state.queryString = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getSearchResults.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(getSearchResults.fulfilled, (state, action) => {
                state.status = 'idle';
                state.data = action.payload;
            });
    },
});

export const { changeQueryString } = searchResultsSlice.actions;

export default searchResultsSlice.reducer;
