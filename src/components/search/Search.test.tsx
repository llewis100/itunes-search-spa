import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../app/store';
import { Search } from './Search';

describe('Search area tests', () => {
    test('renders search heading', () => {
        const { getByText } = render(
            <Provider store={store}>
                <Search />
            </Provider>
        );
    
        expect(getByText(/Search iTunes Music/i)).toBeInTheDocument();
    });
    
    test('renders search input', () => {
        const { container } = render(
            <Provider store={store}>
                <Search />
            </Provider>
        );
        
        expect(container.querySelector('#searchBox')).toBeTruthy()
    });
    
    test('renders search submit button', () => {
        const { container } = render(
            <Provider store={store}>
                <Search />
            </Provider>
        );
    
        expect(container.querySelector('#searchSubmit')).toBeTruthy()
    });
})

