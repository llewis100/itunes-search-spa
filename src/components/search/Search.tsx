import React, { useState } from 'react';
import { useAppDispatch } from '../../app/hooks';
import {
    getSearchResults,
} from './searchSlice';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';

export function Search() {
    const dispatch = useAppDispatch();
    
    const [searchString, setSearchString] = useState('');

    return (
        <div>
            <Box
                sx={{
                    bgcolor: 'background.paper',
                    pt: 8,
                    pb: 6,
                }}
            >
                <Container maxWidth="sm">
                    <Typography
                        component="h1"
                        variant="h2"
                        align="center"
                        color="text.primary"
                        gutterBottom
                    >
                        Search iTunes Music
                    </Typography>
                    <Stack
                        sx={{ pt: 4 }}
                        direction="row"
                        spacing={2}
                        justifyContent="center"
                    >
                        <TextField value={searchString} onChange={(e) => setSearchString(e.target.value)} id="searchBox" />
                        <Button onClick={() => dispatch(getSearchResults(searchString))} id="searchSubmit">Search</Button>
                    </Stack>
                </Container>
            </Box>
        </div>
    );
}
