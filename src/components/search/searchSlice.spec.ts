import searchReducer, {
    searchResultsState,
    changeQueryString
} from './searchSlice';

describe('search reducer', () => {
    const initialState: searchResultsState = {
        queryString: 'this should not be seen on these tests',
        data: [],
        status: 'idle',
    };
    it('should handle initial state', () => {
        expect(searchReducer(undefined, { type: 'unknown' })).toEqual({
            queryString: '',
            data: [],
            status: 'idle',
        });
    });

    it('should handle changeQueryString', () => {
        const actual = searchReducer(initialState, changeQueryString('newString'));
        expect(actual.queryString).toEqual('newString');
    });
});
