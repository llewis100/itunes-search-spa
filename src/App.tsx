import React from 'react';
import { Search } from './components/search/Search';
import { Listing } from './components/listing/Listing';
import './App.css';

function App() {
  return (
    <div className="App">
      <Search />
      <Listing />
    </div>
  );
}

export default App;
